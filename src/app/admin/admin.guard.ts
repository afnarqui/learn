import { EventEmitter, Injectable, Output } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth-service';
import { Storage } from '@ionic/storage'
import { UtilsServices } from '../services/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  @Output() title = new EventEmitter();
  constructor(private authService: AuthService, private router: Router,public storages:Storage,
    public utils: UtilsServices) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      let exists = this.validarRuta(next);
      
      if(this.authService.isLoggedIn && exists){
        return true;
      }
      else{
        return this.redirect();
      }
  }

  private redirect() {
    const navigationExtras: NavigationExtras = {
      state: {
        item: [],
      },
    };
    return this.router.navigate(['home', navigationExtras]);
  }

  async validarRuta(next: ActivatedRouteSnapshot) {
    let nombre = next.routeConfig.path;

    let rutas = await this.storages.get('rut')
    let rutaArray = JSON.parse(rutas)
    let data = 0
    let toolTip = ''
    
    if( rutaArray === null ) {
      this.utils.mensajeMuestro('Advertencia',`Debe iniciar sesión, para ingresar a ${nombre}`,'warning')
      return this.redirect();
    }

    for(var i=0;i < rutaArray.length;i++){
      if(rutaArray[i].nombre == nombre){
        toolTip =  rutaArray[i].toolTip
        data = -1
      }
    }
    
    if(data!=-1){
      this.utils.mensajeMuestro('Advertencia','No tiene permisos para ingresar','warning')
      return this.redirect();
    }else{
    //  this.title.emit(toolTip);
     this.storages.set('title', toolTip);
     return true
    }
  }
}
