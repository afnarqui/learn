import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AdminGuard } from  './admin/admin.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/home/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./components/login/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'eng-card',
    loadChildren: () => import('./components/engCard/eng-card/eng-card.module').then( m => m.EngCardPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./components/home/home/home.module').then( m => m.HomePageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
