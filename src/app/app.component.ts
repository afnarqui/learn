import { Component, OnInit,ViewChild  } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth-service';

import { GraficasServices } from './services/graficas/graficas';
import { CompaniasServices } from './services/companias/companias';
import * as papa from 'papaparse'
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('doughnutCanvas2') doughnutCanvas2;
  doughnutChart: any;
  doughnutChart2: any
  parsedData:any
  parsedDataReal:any
  parseDataHeader:any
  companiasId : any = null
  pushcantidad = 10;
  imageUrl:string =  'assets/imgs/logo.png'
  title = 'San';
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    public servicioCompanias: CompaniasServices,
              public servicio: GraficasServices,
              public routes: Router,
              public storage: Storage
  ) {
    this.initializeApp();
  }
 

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  ngOnInit() {
    this.buscarVentasDiarias()
  }
  async ngAfterViewInit() {
    this.title = await this.storage.get('title');
    this.buscarVentasDiarias()
  }

  buscarVentasDiarias(){
    this.servicioCompanias.devolverVarios(['companiasId'])
    .then((val)=>{
      let companiasId = val['companiasId']
    this.servicio.buscarventasAnualesPorMes(this.companiasId === null ? 1 : this.companiasId)
    .subscribe((data)=>{

      if(data.length == []){
        return
      }
        let valorinicial = []
        let valorfinal = []

        let realvalorgrid = ''
        let realvalorgridHeader = ''
        for(var i = 0; i< data.length;i++){
            valorinicial[i]= data[i]['valorVenta']

            valorfinal[i] = data[i]['horaVenta']
            if(i==0){
              realvalorgrid+= 'hora Venta,valor Venta' + '\n'
            }else{
              realvalorgrid+= data[i]['horaVenta']  + ',' + data[i]['valorVenta']  + '\n'
            }

        }

        this.parsedData = papa.parse(realvalorgrid).data

        this.parseDataHeader =  this.parsedData[0]
        this.parsedData.splice(0,1)
        this.parsedDataReal = this.parsedData
        
        this.graficas(valorinicial,valorfinal)
    },error=>{
        console.log(error)
    })
  })
  }

  graficas(datoss:any,labesl:any){

    // this.doughnutChart2 = new Chart(this.doughnutCanvas2.nativeElement, {

    //     type: 'bar',
    //     data: {
    //         labels: labesl,
    //         datasets: [{
    //             label: datoss,
    //             data:datoss,
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 0.2)',
    //                 'rgba(54, 162, 235, 0.2)',
    //                 'rgba(255, 206, 86, 0.2)',
    //                 'rgba(75, 192, 192, 0.2)',
    //                 'rgba(153, 102, 255, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             hoverBackgroundColor: [
    //                 "#FF6384",
    //                 "#36A2EB",
    //                 "#FFCE56",
    //                 "#FF6384",
    //                 "#36A2EB",
    //                 "#FFCE56"
    //             ]
    //         }]
    //     }

    // });


    //     this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
    //     type: 'bar',
    //     data: {
    //         labels: labesl,
    //         datasets: [{
    //             label: labesl,
    //             data: datoss,
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 0.2)',
    //                 'rgba(54, 162, 235, 0.2)',
    //                 'rgba(255, 206, 86, 0.2)',
    //                 'rgba(75, 192, 192, 0.2)',
    //                 'rgba(153, 102, 255, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             borderColor: [
    //                 'rgba(255,99,132,1)',
    //                 'rgba(54, 162, 235, 1)',
    //                 'rgba(255, 206, 86, 1)',
    //                 'rgba(75, 192, 192, 1)',
    //                 'rgba(153, 102, 255, 1)',
    //                 'rgba(255, 159, 64, 1)'
    //             ],
    //             borderWidth: 1
    //         }]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero:true
    //                 }
    //             }]
    //         }
    //     }

    // });

}
pushMensaje() {

}
cerrarSession(){
  this.auth.logout()
  const navigationExtras: NavigationExtras = {
    state: {
      item: [],
    },
  };
  this.routes.navigate(['login', navigationExtras]);
}
login(){
  this.cerrarSession();
  const navigationExtras: NavigationExtras = {
    state: {
      item: [],
    },
  };
  this.routes.navigate(['login', navigationExtras]);
}
}
