import * as fromUI from './state/ui.reducer';
import * as fromRutas from './state/rut.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
    ui: fromUI.State;
    rutas: fromRutas.RutasState;
}

export const appReducers: ActionReducerMap<AppState> = {
     ui: fromUI.uiReducer,
     rutas: fromRutas.rutasReducer,
};
