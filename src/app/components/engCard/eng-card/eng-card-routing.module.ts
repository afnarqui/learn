import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EngCardPage } from './eng-card.page';

const routes: Routes = [
  {
    path: '',
    component: EngCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EngCardPageRoutingModule {}
