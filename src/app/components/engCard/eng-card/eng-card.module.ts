import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { EngCardPageRoutingModule } from './eng-card-routing.module';

import { EngCardPage } from './eng-card.page';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EngCardPageRoutingModule,
    NgxAudioPlayerModule,
    ReactiveFormsModule,
  ],
  declarations: [EngCardPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class EngCardPageModule {}
