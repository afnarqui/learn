import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EngCardPage } from './eng-card.page';

describe('EngCardPage', () => {
  let component: EngCardPage;
  let fixture: ComponentFixture<EngCardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngCardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EngCardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
