import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseStorageService } from 'src/app/services/firebase/firebase-storage.service';
import { UploadService } from 'src/app/services/upload/upload.service';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { UUID } from 'angular2-uuid';
import { interval, Subscription } from 'rxjs';

export interface ItemCard {
  id: string;
  nombreIngles?: string;
  descripcionIngles?: string;
  nombreEspanol?: string;
  descripcionEspanol?: string;
  url?: string;
  urlImagen?: string;
  color?: string;
  title?: string;
  link?: string;
  artist?: string;
  duration?: number;
}

@Component({
  selector: 'app-eng-card',
  templateUrl: './eng-card.page.html',
  styleUrls: ['./eng-card.page.scss'],
})
export class EngCardPage implements OnInit {
  msaapDisplayTitle = true;
  msaapDisplayPlayList = true;
  msaapPageSizeOptions = [2,4,6];
  msaapDisplayVolumeControls = true;
  msaapDisplayArtist = false;
  msaapDisplayDuration = true;
  msaapDisablePositionSlider = false;
  cardItems;
  listCard: any[] = [];
  listCardAll: any[] = [];
  msaapPlaylist: any[] = [
    {
      id: '1',
      title: 'Audio 1',
      link: 'https://firebasestorage.googleapis.com/v0/b/sistemaadministrativodenegocio.appspot.com/o/1.mp3?alt=media&token=3fee8f9e-58cd-4416-8fc0-34bf9fa45c86',
      artist: 'Audio One Artist',
      duration: 60
    }
  ];
  form: FormGroup;
  error: string;
  userId: number = 1;
  uploadResponse = { status: '', message: '', filePath: '' };

  public archivoForm = new FormGroup({
    archivo: new FormControl(null, Validators.required),
  });
  
  public mensajeArchivo = 'No hay un archivo seleccionado';
  public datosFormulario = new FormData();
  public nombreArchivo = '';
  public URLPublica = '';
  public URLImagen = '';
  public porcentaje = 0;
  public porcentajeImagen = 0;
  public finalizado = false;
  public finalizadoImagen = false;
  public archivo = ''
  private itemsCollectionU: AngularFirestoreCollection<ItemCard>;

  public mensajeArchivoImagen = `No hay una imagen seleccionado`;
  public nombreArchivoImagen =  '';
  @ViewChild('fileInp') fileInput: ElementRef;
  @ViewChild('imagenProducto') imagenProducto: ElementRef;
  imagen
  imagenTemp:string =  'assets/imgs/sinimagen.jpg'
  typeImg
  imagenProd
  isReadyToSave: boolean;
  uuid
  public nombreEspanol = ''
  public nombreIngles = ''
  public descripcionIngles = ''
  public descripcionEspanol = ''
  voltiarImagen = true;
  listaVerde: any[] = [];
  cantidadVerde: number = 0;
  listaRoja: any[] = [];
  cantidadRoja: number = 0;
  listaAzul: any[] = [];
  cantidadAzul: number = 0;
  listaMorado: any[] = [];
  cantidadMorado: number = 0;
  listaAmarillo: any[] = [];
  cantidadAmarillo: number = 0;
  guardando = false;
  reproducir = false;
  subscription: Subscription
  timerVar;
  constructor(private formBuilder: FormBuilder, public uploadService: UploadService, private firebaseStorage: FirebaseStorageService,
              private db: AngularFirestore,) { 
    this.form = formBuilder.group({
      profilePic: [''],
      profilePicIngles: [''],
      nombreIngles: [''],
      descripcionIngles: [''],
      nombreEspanol: [''],
      descripcionEspanol: [''],
      avatar: ['']  
    });

    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

    this.itemsCollectionU = this.db.collection<ItemCard>('/card');
  }

  voltiarImagenEvent() {
    this.reproduccirAudio(this.cardItems.url)
    this.voltiarImagen = !this.voltiarImagen
    // this.nombreEspanol = this.voltiarImagen ? this.cardItems.nombreEspanol : ''
    this.imagen = this.voltiarImagen ? 'assets/imgs/sinimagen.jpg' : this.cardItems.urlImagen
  }

  siguiente() {
    let count =  this.cardItems.count === undefined ? 0 : this.cardItems.count;
    let posicion = this.cardItems.posicion === undefined ? 0: this.cardItems.posicion;
    if(posicion < count ) {
      this.listCard.forEach((value, index, array) => {
        let posicionNueva =  posicion + 1
        if( index === posicionNueva) {
          let valores = this.listCard[index];
          this.llenarValores(valores, count, posicionNueva);
        }
      })
    }
  }

  llenarValores(valores, countArray , index) {
    this.uuid = valores.id;
    this.URLImagen = valores.urlImagen;
    this.nombreIngles = valores.nombreIngles;
    this.descripcionIngles = valores.descripcionIngles;
    this.descripcionEspanol = valores.descripcionEspanol;
    this.URLPublica = valores.url;
    this.imagen = valores.urlImagen;
    this.nombreEspanol = valores.nombreEspanol;
    this.cardItems = {
      id: valores.id,
      url:valores.url,
      urlImagen: valores.urlImagen,
      nombreIngles: valores.nombreIngles,
      descripcionIngles: valores.descripcionIngles,
      nombreEspanol: valores.nombreEspanol,
      descripcionEspanol: valores.descripcionEspanol,
      title: valores.nombreIngles,
      link: valores.url,
      artist: valores.nombreIngles,
      duration: valores.duration,
      count: countArray,
      posicion: index
    }
    this.reproduccirAudio(valores.url);
  }

  anterior() {
    let count =  this.cardItems.count === undefined ? 0 : this.cardItems.count;
    let posicion = this.cardItems.posicion === undefined ? 0: this.cardItems.posicion;
    if(posicion < count && posicion > 0 ) {
      this.listCard.forEach((value, index, array) => {
        let posicionNueva =  posicion - 1
        if( index === posicionNueva) {
          let valores = this.listCard[index];
          this.llenarValores(valores, count, posicionNueva);
        }
      })
    }
  }

  public cambioArchivo(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.mensajeArchivo = `Archivo preparado: ${event.target.files[i].name}`;
        this.nombreArchivo = event.target.files[i].name;
        this.datosFormulario.delete('archivo');
        this.datosFormulario.append('archivo', event.target.files[i], event.target.files[i].name)
      }
    } else {
      this.mensajeArchivo = 'No hay un archivo seleccionado';
    }
  }

  subirArchivoAll() {
  }

  subirArchivoAllImagen() {
    this.subirArchivoImagenNuevo();
  }
  
  public subirArchivoImagenNuevo() {
    let archivo = this.datosFormulario.get('archivo');
    
    let referencia = this.firebaseStorage.referenciaCloudStorage(this.nombreArchivo);
    let tarea = this.firebaseStorage.tareaCloudStorage(this.nombreArchivo, archivo);

    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      if (this.porcentaje == 100) {
        this.finalizado = true;
      }
    });

    referencia.getDownloadURL().subscribe((URL) => {
      this.URLPublica = URL;
      let uuid = UUID.UUID();
      this.uuid = uuid
      
      let items: ItemCard = {
        id: uuid,
        url:this.URLPublica,
        urlImagen: this.URLImagen,
        nombreIngles: this.nombreIngles,
        descripcionIngles: this.descripcionIngles,
        nombreEspanol: this.nombreEspanol,
        descripcionEspanol: this.descripcionEspanol,
        title: this.nombreIngles,
        link: this.URLPublica,
        artist: this.nombreIngles,
        duration: 60
      }
      
      if(this.URLImagen !== '') {
        this.itemsCollectionU.doc(items.id).update(items);
      }
    });


     
    let archivoImagen = this.imagenProd 
    let referenciaImagen = this.firebaseStorage.referenciaCloudStorage( this.imagenProd.name);
    let tareaImagen = this.firebaseStorage.tareaCloudStorage( this.imagenProd.name, archivoImagen);
   
    referenciaImagen.getDownloadURL().subscribe((URL) => {
      let actualizarValores = this.URLPublica !== '' ? true : false;
      let url = this.URLPublica !== '' ? this.URLPublica : '';
      
      let items: ItemCard = {
        id: this.uuid ,
        url,
        urlImagen: URL,
        nombreIngles: this.nombreIngles,
        descripcionIngles: this.descripcionIngles,
        nombreEspanol: this.nombreEspanol,
        descripcionEspanol: this.descripcionEspanol,
        color: '#E6F226',
        title: this.nombreIngles,
        link: url,
        artist: this.nombreIngles,
        duration: 60
      }

      if(actualizarValores){
          this.itemsCollectionU.doc(items.id).set(items);
          this.uuid = ''
          this.cargarAudio();
      } else {
        
        this.itemsCollectionU.doc(items.id).set(items);
      }
    });

    tareaImagen.percentageChanges().subscribe((porcentaje) => {
      this.porcentajeImagen = Math.round(porcentaje);
      if (this.porcentajeImagen == 100) {
        this.finalizadoImagen = true;
      }
    });


  }

  cargarAudio() {
    this.msaapPlaylist = []
      const req = this.db
      .collection('/card')
      .ref.where('id', '!=', '1');
    return req.get().then((result) => {
      return result.forEach((doc: any) => {
        const valores = doc.data();
        this.msaapPlaylist.push({
          id: valores.id,
          title: valores.title,
          link: valores.link,
          artist: valores.artist,
          duration: valores.duration
        })

        this.listCard.push({
          id: valores.id,
          title: valores.title,
          link: valores.link,
          artist: valores.artist,
          duration: valores.duration,
          nombreIngles: valores.nombreIngles,
          descripcionIngles: valores.descripcionIngles,
          nombreEspanol: valores.nombreEspanol,
          descripcionEspanol: valores.descripcionEspanol,
          url: valores.url,
          urlImagen: valores.urlImagen,
          color: valores.color
        })
        this.cantidadColores(valores);
      });
    });


  }


  public colores(color) {
    let items = this.cardItems
    if(items !== undefined){
      items.color = color
      this.itemsCollectionU.doc(items.id).update(items);
      this.limpiarCantidades();
      this.cargarAudio();
      this.buscarItemsPorColor(color);
    }
  }

  limpiarCantidades() {
      this.cantidadAmarillo = 0
      this.cantidadMorado = 0
      this.cantidadAzul = 0
      this.cantidadRoja = 0
      this.cantidadVerde = 0
      this.listCard = []
  }

  guardarValores() {
    this.guardando = !this.guardando
  }

  buscarItemsPorColor(color) {
      let copia: any[] = [];
      if(this.listCard.length === 0) {
        this.listCard = this.listCardAll;
      }
      this.listCard.forEach((value, index, array) => {
        if( this.listCard[index].color === color) {
          copia.push({
            id: this.listCard[index].id,
            title: this.listCard[index].title,
            link: this.listCard[index].link,
            artist: this.listCard[index].artist,
            duration: this.listCard[index].duration
          })
        }
      })
      if(copia.length > 0) {
        this.msaapPlaylist = copia;
      }
  }

  public subirArchivo() {
    let archivo = this.datosFormulario.get('archivo');
    
    let referencia = this.firebaseStorage.referenciaCloudStorage(this.nombreArchivo);
    let tarea = this.firebaseStorage.tareaCloudStorage(this.nombreArchivo, archivo);


    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      if (this.porcentaje == 100) {
        this.finalizado = true;
      }
    });

    referencia.getDownloadURL().subscribe((URL) => {
      this.URLPublica = URL;
      let uuid = UUID.UUID();
      this.uuid = uuid
      
      let items: ItemCard = {
        id: uuid,
        url:this.URLPublica,
        urlImagen: this.URLImagen,
        nombreIngles: this.nombreIngles,
        descripcionIngles: this.descripcionIngles,
        nombreEspanol: this.nombreEspanol,
        descripcionEspanol: this.descripcionEspanol
      }
      
      if(this.URLImagen !== '') {
        this.itemsCollectionU.doc(items.id).update(items);
      } else {
        this.itemsCollectionU.doc(items.id).set(items);
      }
    });

  }


  public subirArchivoImagen() {
   
    let archivoImagen = this.imagenProd
    let referenciaImagen = this.firebaseStorage.referenciaCloudStorage( this.imagenProd.name);
    let tareaImagen = this.firebaseStorage.tareaCloudStorage( this.imagenProd.name, archivoImagen);
   
    referenciaImagen.getDownloadURL().subscribe((URL) => {
      let actualizarValores = this.URLPublica !== '' ? true : false;
      let url = this.URLPublica !== '' ? this.URLPublica : '';
      
      this.URLPublica = URL;

      let items: ItemCard = {
        id: this.uuid ,
        url,
        urlImagen: this.URLImagen,
        nombreIngles: this.nombreIngles,
        descripcionIngles: this.descripcionIngles,
        nombreEspanol: this.nombreEspanol,
        descripcionEspanol: this.descripcionEspanol,
        color: '#1F7C22' 
      }

      if(actualizarValores){
        this.itemsCollectionU.doc(items.id).update(items);
      } else {
        this.itemsCollectionU.doc(items.id).set(items);
      }
    });

    tareaImagen.percentageChanges().subscribe((porcentaje) => {
      this.porcentajeImagen = Math.round(porcentaje);
      if (this.porcentajeImagen == 100) {
        this.finalizadoImagen = true;
      }
    });
  }

  ngOnInit() {
    this.cargarAudio();
    this.cargarAll();
  }

  cargarAll() {
      const req = this.db
      .collection('/card')
      .ref.where('id', '!=', '1');
    return req.get().then((result) => {
      return result.forEach((doc: any) => {
        const valores = doc.data();
        this.listCardAll.push({
          id: valores.id,
          title: valores.title,
          link: valores.link,
          artist: valores.artist,
          duration: valores.duration,
          nombreIngles: valores.nombreIngles,
          descripcionIngles: valores.descripcionIngles,
          nombreEspanol: valores.nombreEspanol,
          descripcionEspanol: valores.descripcionEspanol,
          url: valores.url,
          urlImagen: valores.urlImagen,
          color: valores.color
        })
      });
    });
 }

  onEnded(event) {}

  reproduccirAudio(name) {
    let nombreAudio = name;
    var audioel = document.getElementById('audioElem');
    if (audioel !== null) {
      audioel.setAttribute('src', `${nombreAudio}`);
      audioel.setAttribute('type','audio/mpeg')
    }
    var audio = new Audio(`${nombreAudio}`);

    audio.play();
  }

  valoresIniciales() {
    this.msaapPlaylist.forEach((value, index, array) => {
      if( index === 0) {
        let id = this.msaapPlaylist[index].id;
        const req = this.db
        .collection('/card')
        .ref.where('id', '==', id);
      return req.get().then((result) => {
        return result.forEach((doc: any) => {
          const valores = doc.data();
          this.cantidadColores(valores);
          this.llenarValores(valores, index, index);
        });
      });

      }  
    })
  }

  cantidadColores(valores) {
    if(valores.color === '#E6F226') {
      this.cantidadAmarillo += 1;
     }
     if(valores.color === '#A20EBF') {
      this.cantidadMorado += 1;
    }
    if(valores.color === '#254DF5') {
      this.cantidadAzul += 1;
    }
    if(valores.color === '#F52525') {
      this.cantidadRoja += 1;
    }
    if(valores.color === '#1F7C22') {
      this.cantidadVerde += 1
    }
  }

  trackClick(event, item){
    let currentIndex = item.currentIndex
    this.voltiarImagen = false
    let countArray = this.msaapPlaylist.length
    this.msaapPlaylist.forEach((value, index, array) => {
      if( index === currentIndex) {
        let id = this.msaapPlaylist[index].id;
        const req = this.db
        .collection('/card')
        .ref.where('id', '==', id);
      return req.get().then((result) => {
        return result.forEach((doc: any) => {
          const valores = doc.data();
          this.llenarValores(valores, countArray, index);
        });
      });

      }  
    })
    
  }
  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('avatar').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('file', this.form.get('avatar').value);
    this.uploadService.uploadImage(this.datosFormulario, this.form.get('avatar').value);
  }


  public cambioArchivoImagen(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.mensajeArchivoImagen = `Imagen preparado: ${event.target.files[i].name}`;
        this.nombreArchivoImagen = event.target.files[i].name;
        this.datosFormulario.delete('archivoImagen');
        this.datosFormulario.append('archivoImagen', event.target.files[i], event.target.files[i].name)
      }
    } else {
      this.mensajeArchivo = 'No hay una imagen seleccionado';
    }
  }

  fileUpload(event) {
    var reader = new FileReader();
    
    reader.onload = (e) => {
      this.imagenProducto['src'] = e.target['result'];
      this.imagen = e.target['result'];
    };

    reader.readAsDataURL(event.srcElement.files[0]);
    this.typeImg = event.srcElement.files[0].type
    this.imagenProd = event.srcElement.files[0]
    this.mensajeArchivoImagen = `Imagen preparado: ${event.target.files[0].name}`;
    this.nombreArchivoImagen = event.target.files[0].name;
    this.datosFormulario.delete('archivoImagen');
  }  

  onClick() {
    this.fileInput.nativeElement.click();
  }

  reproduccirTodo() {
    this.reproducir = !this.reproducir
    if(this.reproducir) {
      let items = this.cardItems === undefined ? [] : this.cardItems
      let inicio = 1
      if (items.length === 0 && this.listCardAll.length > 0) {
        this.llenarValores(this.listCardAll[0], inicio, inicio);
        this.startTimer()
      } else {
        this.startTimer()
    }
    }else {
      this.subscription.unsubscribe();
    }
  }

  startTimer(){
    this.timerVar = interval(9000);
    this.subscription = this.timerVar.subscribe(x => this.recorrer());
}


  recorrer() {
        let count =  this.listCardAll.length;
        let posicion = this.cardItems.posicion;
        let donde = posicion + 1
        let inicio = 1
        if(posicion < count ) {
          if(donde > count){
            this.llenarValores(this.listCardAll[0], count, inicio);
          } else {
            this.llenarValores(this.listCardAll[donde - 1],donde, donde);
          }
        } else {
          this.llenarValores(this.listCardAll[0], inicio, inicio);
        }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  agregarMinutos(minutos: number) {
    let fecha = new Date();
    let sumarsesion = minutos;
    let seconds = fecha.getSeconds();
    fecha.setSeconds(seconds + sumarsesion);
    return  fecha;
  }

}
