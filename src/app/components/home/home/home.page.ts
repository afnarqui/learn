import { Component, OnInit,ViewChild } from '@angular/core';
import { GraficasServices } from '../../../services/graficas/graficas';
import { CompaniasServices } from '../../../services/companias/companias';
import * as papa from 'papaparse'
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from '../../../services/auth-service';
import { Storage } from '@ionic/storage'
import { UtilsServices } from 'src/app/services/utils/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  doughnutChart: any;
  doughnutChart2: any
  parsedData:any
  parsedDataReal:any
  parseDataHeader:any
  companiasId : any = null
  pushcantidad = 10;
  imageUrl:string =  'assets/imgs/logo.png'
  constructor(public servicioCompanias: CompaniasServices,
              public servicio: GraficasServices,
              public routes: Router,
              public auth: AuthService,
              public storage: Storage,
              public utils: UtilsServices) { }

  ngOnInit() {
    this.buscarVentasDiarias()
  }
  ngAfterViewInit() {
    this.buscarVentasDiarias()
  }

  irApage(name) {
    // if(!this.auth.isLoggedIn) {
    //   this.utils.mensajeMuestro('Advertencia',`Debe iniciar sesión, para ingresar a ${name}`,'warning')
    //   return this.redirect("home");
    // } else {
    //   this.redirect(name);
    // }
    this.redirect(name);
  }

  redirect(name) {
    const navigationExtras: NavigationExtras = {
      state: {
        item: [],
      },
    };
    this.routes.navigate([name, navigationExtras]);
  }

  buscarVentasDiarias(){
    this.servicioCompanias.devolverVarios(['companiasId'])
    .then((val)=>{
      let companiasId = val['companiasId']
    this.servicio.buscarventasAnualesPorMes(this.companiasId === null ? 1 : this.companiasId)
    .subscribe((data)=>{

      if(data.length == []){
        return
      }
        let valorinicial = []
        let valorfinal = []

        let realvalorgrid = ''
        let realvalorgridHeader = ''
        for(var i = 0; i< data.length;i++){
            valorinicial[i]= data[i]['valorVenta']

            valorfinal[i] = data[i]['horaVenta']
            if(i==0){
              realvalorgrid+= 'hora Venta,valor Venta' + '\n'
            }else{
              realvalorgrid+= data[i]['horaVenta']  + ',' + data[i]['valorVenta']  + '\n'
            }

        }

        this.parsedData = papa.parse(realvalorgrid).data

        this.parseDataHeader =  this.parsedData[0]
        this.parsedData.splice(0,1)
        this.parsedDataReal = this.parsedData
        
        this.graficas(valorinicial,valorfinal)
    },error=>{
        console.log(error)
    })
  })
  }

  graficas(datoss:any,labesl:any){



}
pushMensaje() {

}
cerrarSession(){
  this.auth.logout()
  const navigationExtras: NavigationExtras = {
    state: {
      item: [],
    },
  };
  this.routes.navigate(['login', navigationExtras]);
}
login(){
  const navigationExtras: NavigationExtras = {
    state: {
      item: [],
    },
  };
  this.routes.navigate(['login', navigationExtras]);
}
}
