import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth-service';
import { User } from '../../../models/user';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { CompaniasServices } from '../../../services/companias/companias'
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { menuList } from '../../../models/menu-list';
import { UtilsServices } from '../../../services/utils/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  seleccionarCompania: boolean = false
  companiasAcesso = []
  usuario: string;
  clave: string;
  user = {} as User;
  imageUrl:string =  'assets/imgs/logo.png'
  sideMenu = menuList;
  constructor(public auth: AuthService,
              public storage: Storage,
              public loadingController: LoadingController,
              public servicioCompanias: CompaniasServices,
              public routes: Router,
              private route: ActivatedRoute,
              public utils: UtilsServices) {}


  ngOnInit() {

  }
  
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }

  async login() {
    try {
      let credenciales = {
        usuario: this.user.usuario,
        clave: this.user.clave
      }

      this.companiasAcesso = []
      this.auth.login(credenciales).subscribe(
        (res: any) => {

          this.seleccionarCompania = true

          for (let index = 0; index < res.length; index++) {
            const nombre = res[index]['nombreCompania']
            const companiaId = res[index]['companiasId']
            this.companiasAcesso.push({id: companiaId, nombre:nombre, obj:res[index]})
          }
          this.companiasAcesso.pop();
          let posic = res.length - 1
          let datosrut = JSON.stringify(res[posic])
          let rutasNuevas = JSON.parse(datosrut);
          this.llenarValoresRutas(rutasNuevas, this.sideMenu);
          let data=[{valor:0}]
          this.storage.set('data',data)
          this.presentLoading();
        },
        error => {
          this.presentLoading();
          var message = JSON.parse(error['_body'])
          this.utils.mensajeMuestro('Error', message['message'], 'error');
        }
      )

    } catch (error) {
      this.presentLoading();
      this.utils.mensajeMuestro('Error', error['message'], 'error');
    }
  }

  limpiar() {

  }

  llenarValoresRutas(rutas, data) {
    
    for(let i = 0; i < rutas.length;i++) {
      for(let k = 0; k < data.length; k++) {
        if(data[k].route === rutas[i].nombre){
          rutas[i]['toolTip'] = data[k].toolTip
        } else {
          rutas[i]['toolTip'] = ''
        }
      }
    }
    
    let nuevosValores = JSON.stringify(rutas)
    this.storage.set('rut',nuevosValores)
  }

  ingresarEnCompania(obj) {
    this.auth.setVarsSession(obj)
    this.servicioCompanias.cargarcompaniaId(obj.companiasId)
    const navigationExtras: NavigationExtras = {
      state: {
        item: [],
      },
    };
    this.routes.navigate(['home', navigationExtras]);
  }
  atras() {
    const navigationExtras: NavigationExtras = {
      state: {
        item: [],
      },
    };
    this.routes.navigate(['home', navigationExtras]);
  }
  
  ngAfterViewInit() {
    this.seleccionarCompania = false;
  }

}
