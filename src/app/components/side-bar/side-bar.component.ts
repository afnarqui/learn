import { Component, OnInit } from '@angular/core';
import { menuList } from '../../models/menu-list';
import { AuthService } from '../../services/auth-service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  sideMenu = menuList;
  collapse = true;
  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

  toggleSidebar() {
    this.collapse = !this.collapse;
  }
}
