import { Component } from '@angular/core';
import { AuthService } from '../../services/auth-service';
import { User } from '../../models/user';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { CompaniasServices } from '../../services/companias/companias'
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  seleccionarCompania: boolean = false
  companiasAcesso = []
  usuario: string;
  clave: string;
  user = {} as User;
  constructor(public auth: AuthService,
              public storage: Storage,
              public loadingController: LoadingController,
              public servicioCompanias: CompaniasServices,
              public routes: Router,
              private route: ActivatedRoute,) {}

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: null,
      duration: 5000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }

  async login() {
    try {
      let credenciales = {
        usuario: this.user.usuario,
        clave: this.user.clave
      }

      this.companiasAcesso = []
      this.auth.login(credenciales).subscribe(
        (res: any) => {

          this.seleccionarCompania = true

          for (let index = 0; index < res.length; index++) {
            const nombre = res[index]['nombreCompania']
            const companiaId = res[index]['companiasId']
            this.companiasAcesso.push({id: companiaId, nombre:nombre, obj:res[index]})
          }
          this.companiasAcesso.pop();
          let posic = res.length - 1
          let datosrut = JSON.stringify(res[posic])
          this.storage.set('rut',datosrut)
          let data=[{valor:0}]
          this.storage.set('data',data)
          this.presentLoading();
        },
        error => {
          this.presentLoading();
          // var message = JSON.parse(error['_body'])
          // this.showAlert(message['message']);
        }
      )

    } catch (error) {
      this.presentLoading();
      // this.showAlert(error['message']);
      console.log(error);
    }
  }

  limpiar() {

  }

  ingresarEnCompania(obj) {
    this.auth.setVarsSession(obj)
    this.servicioCompanias.cargarcompaniaId(obj.companiasId)
    const navigationExtras: NavigationExtras = {
      state: {
        item: [],
      },
    };
    this.routes.navigate(['tabs/san-home', navigationExtras]);
  }
}
