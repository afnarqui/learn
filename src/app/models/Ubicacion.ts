export interface Ubicacion {
    ubicacionId?:string,
    nombreUbicacion:string,
    contenedorId:string,
    queHago:string,
    nombreTabla:string,
    name:string,
    companiasId:string,  
}