export interface clientes {
    tipoIdentificacion:string,
    identificacion:string,
    digito:string,
    fechaExpedicion:string
    primerApellido:string,
    segundoApellido:string,
    primerNombre:string,
    segundoNombre:string
    nombreCompleto:string,
    fechaIngreso:string,
    estado:string,
    clientesId?:string,
    queHago:string,
    nombreTabla:string,
    name:string,
    numeroTelefono :string,
    direccion :string
}