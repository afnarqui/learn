export interface clientesCorreos {
    clientesCorreosId?:string,
    correoElectronico:string,
    clientesId:string,
    nombreCompleto?:string,
    queHago:string,
    nombreTabla:string,
    name:string
}
