export interface clientesTelefonos {
    clientesTelefonosId?:string,
    numeroTelefono:string,
    clientesId:string,
    nombreCompleto?:string,
    queHago:string,
    nombreTabla:string,
    name:string
}