export interface  IComandasProductosDinamicos {
        comandasProductosDinamicosId?: number;
        companiasId: number;
        productosPorgruposId: number;
        productosId: number;
        tableDomiciliosAppDescripcionUuid: string;
        turno: number;
        tipo: string;
        nombre: string;
        cantidad: number;
        valorVenta: number;
        fecha: string;
        estado: string;
        codigo: string;
        descripcion: string;
}
