import { IComandasProductosDinamicos } from './comandasProductosDinamicos.interface';

export class ComandasProductosDinamicos {
        public comandasProductosDinamicosId?: number;
        public companiasId: number;
        public productosPorgruposId: number;
        public productosId: number;
        public tableDomiciliosAppDescripcionUuid: string;
        public turno: number;
        public tipo: string;
        public nombre: string;
        public cantidad: number;
        public valorVenta: number;
        public fecha: string;
        public estado: string;
        public codigo: string;
        public descripcion: string;
    constructor(data: IComandasProductosDinamicos) {
        this.comandasProductosDinamicosId = data && data.comandasProductosDinamicosId || null;
        this.companiasId = data && data.companiasId || null;
        this.productosPorgruposId = data && data.productosPorgruposId || null;
        this.productosId = data && data.productosId || null;
        this.tableDomiciliosAppDescripcionUuid = data && data.tableDomiciliosAppDescripcionUuid || null;
        this.turno = data && data.turno || null;
        this.tipo = data && data.tipo || null;
        this.nombre = data && data.nombre || null;
        this.cantidad = data && data.cantidad || null;
        this.valorVenta = data && data.valorVenta || null;
        this.fecha = data && data.fecha || null;
        this.estado = data && data.estado || null;
        this.codigo = data && data.codigo || null;
        this.descripcion = data && data.descripcion || null;
    }
  }

