export interface cuadreCajaDiarioValoresPorDenominacion{
    cuadreCajaDiarioValoresPorDenominacionId?: string,
    fechaCuadre?: string,
    usuarioCuadro?: string,
    companiasId?: string,
    denominacionesId?: string,
    cantidad?: string
}