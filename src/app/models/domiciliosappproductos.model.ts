export class DomicilioAppProductos {
  cantidad:number;
  domiciliosAddGruposProductosContenidoId:string;
  domiciliosAddgruposUuid:string;
  domiciliosAddproductosUuid:string;
  imagen:string;
  nombre:string;
  productosId:string;
  valorVenta:number;
  valoresAdicionales:boolean;

  constructor(
    cantidad:number,
    domiciliosAddGruposProductosContenidoId:string,
    domiciliosAddgruposUuid:string,
    domiciliosAddproductosUuid:string,
    imagen:string,
    nombre:string,
    productosId:string,
    valorVenta:number,
    valoresAdicionales:boolean
  ) {
    this.cantidad = cantidad;
    this.domiciliosAddGruposProductosContenidoId = domiciliosAddGruposProductosContenidoId;
    this.domiciliosAddgruposUuid = domiciliosAddgruposUuid;
    this.domiciliosAddproductosUuid = domiciliosAddproductosUuid;
    this.imagen = imagen;
    this.nombre = nombre;
    this.productosId = productosId;
    this.valorVenta = valorVenta;
    this.valoresAdicionales = valoresAdicionales;
  }
}

