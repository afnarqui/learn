 export class GestionCaja {
  companiasId: number;
  usuariosId: number;
  clientesId: number;
  codigo: string;
  numero: number;
  descripcion: string;

  constructor (
               companiasId: number,
               usuariosId: number,
               clientesId: number,
               codigo: string,
               numero: number,
               descripcion: string
              ) {
        this.companiasId = companiasId;
        this.codigo = codigo;
        this.numero = numero;
        this.usuariosId = usuariosId;
        this.clientesId = clientesId;
        this.descripcion = descripcion;
  }
}

