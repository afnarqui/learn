export interface lotes{
    lotesId?: string,
    codigoLote?:string,
    fechaElaboracion?: string,
    fechaCaducidad:string,
    productosId?: string,
    cantidadInicial?: string,
    estado?: string,
    companiasId?:string,
    queHago?: string,
    name?: string,
    nombreTabla?: string
}