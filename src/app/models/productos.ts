export interface productos {
    productosId:number
	codigoProducto:string
	nombreProducto:string
	descripcion:string
	imagen:string
	costo:number
	valorVenta:number
	existenciasMinimas:number
	existenciasActuales:number
	visualizaEnCaja:string
	controlaEnInventario:string
	companiasId:number
}