export interface productosElaboradosModelo {
    codigoProductoElaborado?: string,
    nombreProductoElaborado?: string,
    imagen?: string,
    valorVenta?: string,
    companiasId?: string,
    tipoImpuesto?: string,
    valorImpuesto?: string
}
