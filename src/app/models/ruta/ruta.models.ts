import { IRuta } from './ruta.interface';

export class Rutas {
    public nombre: string;
   
    constructor(data: IRuta) {
        this.nombre = data && data.nombre || null;
        }
  }

