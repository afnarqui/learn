export interface seguimientoIngresosStock{
    seguimientoIngresosStockId?: string,
    proveedoresId?: string,
    productosId?: string,
    cantidad?: string,
    fechaIngreso?: string,
    usuarioIngreso?: string,
    costoUnitario?: string,
    queHago?: string,
    name?: string,
    nombreTabla?: string,
    cerradoEnCaja?:string,
    recibo?:string
}