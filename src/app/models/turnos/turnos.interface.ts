export interface  ITurnos {
    domiciliosAddGruposProductosContenidoDescuentoInventarioUuid: string;
    companiasId: number;
    clientesId: number;
    usuariosId: number;
    estado: string;
    turno: number;
    cantidad: number;
    fecha: string;
    domicilio: boolean;
    codigo: string;
    numeroPedido: number;
}