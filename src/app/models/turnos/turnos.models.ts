import { ITurnos } from './turnos.interface';

export class Turnos {
    public domiciliosAddGruposProductosContenidoDescuentoInventarioUuid: string;
    public companiasId: number;
    public clientesId: number;
    public usuariosId: number;
    public estado: string;
    public turno: number;
    public cantidad: number;
    public fecha: string;
    public domicilio: boolean;
    public codigo: string;
    public numeroPedido: number;
    
    constructor(data: ITurnos) {
        this.domiciliosAddGruposProductosContenidoDescuentoInventarioUuid = data && data.domiciliosAddGruposProductosContenidoDescuentoInventarioUuid || null;
        this.companiasId = data && data.companiasId || null;
        this.clientesId = data && data.clientesId || null;
        this.usuariosId = data && data.usuariosId || null;
        this.estado = data && data.estado || null;
        this.turno = data && data.turno || null;
        this.cantidad = data && data.cantidad || null;
        this.fecha = data && data.fecha || null;
        this.domicilio = data && data.domicilio || null;
        this.codigo = data && data.codigo || null;
        this.numeroPedido = data && data.numeroPedido || null;
    }
  }

