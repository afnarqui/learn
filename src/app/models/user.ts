export interface User {
    usuario: string;
    clave: string;
    nombreCompania: string;
    companiasId: number;
}