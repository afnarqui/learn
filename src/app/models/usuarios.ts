export interface usuarios {
    clave:string,
	codigoUsuario:string,
	nombreUsuario:string,
	cargosId:string,
	administrador:number,
	correoElectronico:string,
	intentosClaves:string,
	ultimoCambioClave:string,
	diasCambioClave:number,
	cambiaClaveProximoInicioSesion:number,
	estado:number
}