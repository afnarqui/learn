import { Pipe, Injectable,PipeTransform } from '@angular/core';


@Pipe({
  name: 'abc',
})
@Injectable()
export class AbcPipe implements PipeTransform {
  transform(productos, grupoId) {

    if( !grupoId || grupoId == 0 || grupoId == '') {
      return productos
    }
  
 
      let productosCoincidencias = productos.filter( item => item.nombreProducto.charAt(0) === grupoId) 
   
      return productosCoincidencias;
  
    
  }
}




