import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroProductos'
})
@Injectable()
export class filtroProductos implements PipeTransform {

  transform(productos, grupoId) {
    // si el filtro no esta definido retorna todos los productos
    if( !grupoId || grupoId == 0 || grupoId == '') {
      return productos
    }
 
      // filtrar los productos con el grupoId
      let productosCoincidencias = productos.filter( item => item.gruposProductosId === grupoId) 
      return productosCoincidencias;
    
    
  }
}