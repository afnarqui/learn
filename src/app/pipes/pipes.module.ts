import { NgModule } from "@angular/core"
import { filtroProductos } from './filtroProductos';
import { valorTotalVenta } from './valorTotalVenta'
import { OrderByPipe } from './order-by/order-by';
import { AbcPipe } from './abc/abc';

@NgModule({
  declarations: [
    filtroProductos,
    valorTotalVenta,
    OrderByPipe,
    AbcPipe,
  ],
  exports: [
    filtroProductos,
    valorTotalVenta,
    OrderByPipe,
    AbcPipe,
  ]
})
export class PipesModule {
}