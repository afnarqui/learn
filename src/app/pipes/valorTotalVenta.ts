import { Pipe, Injectable, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valorTotalVenta'
})
@Injectable()
export class valorTotalVenta implements PipeTransform {

  transform(productos) {
    return productos.reduce((a, b) => a + (b['valorVenta'] * b['cantidad']), 0);
  }
}