import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn = true;
  public token: string;
  constructor(
    private http: HttpClient,
    private storage: Storage,
  ) { 
    this.storage.get('token').then((val) => {
      this.token = val;
  });
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  /**
   * @method login este metodo se encarga de autenticar y retornar las agencias y el token
   * @param credenciales parametros clave y usuarios para autenticar
   */
  login(credenciales) {
    const url = environment.URL_SERVICIOS_LOCALES;

    return this.http.post(`${url}/login`, credenciales, this.httpOptions);
  }

 getToken() {
    return this.token;
  }
  /**
   * @method setVarsSession se encarga se hacer set en el session storage de todas las variables de session
   * @param obj objecto con todas las variables de session
   */
  setVarsSession(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        const element = obj[key];
        this.storage.set(key, element);
      }
    }
    this.isLoggedIn = true;
  }

  // verificar si esta autenticado
  authenticated() {
    return this.storage.get('token').then((val) => {
      if (val) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      return this.isLoggedIn;
    });
  }

  /**
   * @method logout este metodo se encarga de limpiar todas las varibles de session
   */
  logout() {
    this.storage.remove('companiasId');
    this.storage.remove('token');
    this.storage.remove('usuariosId');
    this.storage.remove('rut');
    this.storage.remove('nombreUsuario');
    this.storage.remove('nombreCompania');
    this.storage.remove('nombreCargo');
    this.storage.remove('data');
    this.storage.remove('codigousuario');
    this.storage.remove('codigoCompania');
    this.storage.remove('cargosId');
    this.storage.remove('title');
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }
}
