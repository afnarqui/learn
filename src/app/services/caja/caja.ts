import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';

@Injectable({
    providedIn: 'root'
})
export class CajaServices {
    token: string;
    urlPrintService: string;
    companiaId: number;
    usuarioId: number;
    
    constructor(public http: HttpClient, private storage: Storage,
                private store: Store<AppState>
                ) {
        this.storage.get('token').then((val) => {
            this.token = val;
        });

        this.storage.get('companiasId').then((val) => {
            this.companiaId = val;
        });

        this.storage.get('urlPrint').then((val) => {
            this.urlPrintService = val;
        });

        this.storage.get('usuariosId').then((usuario) => {
            this.usuarioId = usuario;
        });

    }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    httpOptionsToken = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line: max-line-length
        authorization: 'Bearer CiAgICAgICAgICAgICAgU0FSQWBTCiAgICAgICAgICBOSVQ6IDQzNjE0MzM3LTkKICAgICAgIFJFR0lNRU4gU0lNUExJRklDQURPCiAgICAgICBESVI6IENSIDgzICMgMzIgQiAxNwogICAgICAgICAgVEVMOiAyOTQgMTIgOTEKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKRmFjdHVyYSBOcm8uICAgOiAxOTQ3CkZlY2hhIHZlbnRhICAgIDogMjAxOC0xMS0yOSAyMzozOQpVc3VhcmlvICAgICAgICA6IFNBUkEKQ2xpZW50ZTogQ0xJRU5URSBPQ0FTSU9OQUwKSWRlbnRpZmljYWNpw7NuOiA5OTk5CkRpcjogClRlbDogCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCk5vbWJyZSBQcm9kdWN0byAgICAgICAgICAgICAgICAgICAgCkNhbnQuICAgIFZhbG9yIFVuaS4gICAgIFZhbG9yIFRvdGFsCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkdBU0VPU0EgMjUwICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAzICAgICAgMi4wMDAsMDAgICAgICAgIDYuMDAwLDAwClBJTFNFTiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwCkFHVUlMQSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwClVOSURBRCBCT1NUT04gLSBHUkVFTiAgICAgICAgICAgICAgCiAgICA0ICAgICAgICA1MDAsMDAgICAgICAgIDIuMDAwLDAwCk1JQ0hFTEFEQSBQSUxTRU4gICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC41MDAsMDAgICAgICAgIDQuNTAwLDAwCkNMSUNMRSAgUEVRVUXDkU8gQUREQU1TICAgICAgICAgICAgIAogICAgNCAgICAgICAgMjAwLDAwICAgICAgICAgIDgwMCwwMApWSVZFIDEwMCBQRVFVRcORTyAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICAyLjAwMCwwMCAgICAgICAgNC4wMDAsMDAKVklWRSAxMDAgR1JBTkRFICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAyLjUwMCwwMCAgICAgICAgMi41MDAsMDAKR0FTRU9TQSAgUE9TVE9CT04gMS41ICAgICAgICAgICAgICAKICAgIDMgICAgICA0LjAwMCwwMCAgICAgICAxMi4wMDAsMDAKQk9URUxMQSBBR1VBICAgICAgICAgICAgICAgICAgICAgICAKICAgIDcgICAgICAzLjAwMCwwMCAgICAgICAyMS4wMDAsMDAKVElOVE8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAyLjAwMCwwMCAgICAgICAgMi4wMDAsMDAKR0FTRU9TQSA0MDAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAzLjAwMCwwMCAgICAgICAgMy4wMDAsMDAKR0FUT1JBREUgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICA0LjAwMCwwMCAgICAgICAgNC4wMDAsMDAKQUdVQSBTQUJPUklaQURBIEgyTyAgICAgICAgICAgICAgICAKICAgIDEgICAgICA0LjAwMCwwMCAgICAgICAgNC4wMDAsMDAKQ0FTRVJBUyAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICA0LjAwMCwwMCAgICAgICAgOC4wMDAsMDAKQk9NQk9NQlVNICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAgIDUwMCwwMCAgICAgICAgICA1MDAsMDAKSEVJTkVLRU4gICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICA3LjAwMCwwMCAgICAgICAxNC4wMDAsMDAKMS8yIExVQ0tZICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICA1LjAwMCwwMCAgICAgICAgNS4wMDAsMDAKVFJJREVOVCBJTkRJVklEVUFMICAgICAgICAgICAgICAgICAKICAgIDIgICAgICAgIDMwMCwwMCAgICAgICAgICA2MDAsMDAKQlJFVEHDkUEgTk8gUkVUT1JOQUJMRSAgICAgICAgICAgICAgCiAgICAxICAgICAgMy4wMDAsMDAgICAgICAgIDMuMDAwLDAwClVOSURBRCBMVUNLWSAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgICA2MDAsMDAgICAgICAgICAgNjAwLDAwCkhFUlNIRVlTICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwClRSSURFTlQgWCAzICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMS4wMDAsMDAgICAgICAgIDEuMDAwLDAwCkNBTkRFTEEgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMi4wMDAsMDAgICAgICAgIDIuMDAwLDAwClJPTiBTRU5DSUxMTyAgICAgICAgICAgICAgICAgICAgICAgCiAgICAyICAgICAgNS4wMDAsMDAgICAgICAgMTAuMDAwLDAwCkdBU0VPU0EgMzUwICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMy4wMDAsMDAgICAgICAgIDMuMDAwLDAwClNPREEgTUlDSEVMQURBICAgICAgICAgICAgICAgICAgICAgCiAgICAyICAgICAgNC4wMDAsMDAgICAgICAgIDguMDAwLDAwCjEvMiBCT1NUT04gLSBHUkVFTiAgICAgICAgICAgICAgICAgCiAgICA3ICAgICAgNC4wMDAsMDAgICAgICAgMjguMDAwLDAwClVOSURBRCBNQUxCT1JPICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgICA2MDAsMDAgICAgICAgICAgNjAwLDAwClNBTCBERSBGUlVUQVMgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMy41MDAsMDAgICAgICAgIDMuNTAwLDAwCkFMS0FTRUxUWkVSICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMi41MDAsMDAgICAgICAgIDIuNTAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tClNVQlRPVEFMICAgICAgICAgICA1OSAgICAxNjguMDYwLDAwCklNUE9DT05TVU1PIDEsMDAlICAgICAgICAgICAgIDQwLDAwClRPVEFMIEEgUEFHQVIgICAgICAgICAgICAxNjguMTAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkVGRUNUSVZPICAgICAgICAgICAgICAgICAxNjguMTAwLDAwCkNBTUJJTyAgICAgICAgICAgICAgICAgICAgICAgICAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkdyYWNpYXMgcG9yIHN1IENvbXByYQogICAgSW1wcmVzbyBwb3IgUmVzdEFwcC4KICAgICAoKzU3KSAzMjAgNzMzMDI1NAogIGFuZHJlc25hcmFuam9AYWZuYXJxdWkuY29tCgoK'
    })
    };

    buscarGruposProductosParaCaja(companiaId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/proceso/gruposProductosParaCaja?companiaId=${companiaId}`);
    }

    productosAgrupados(companiaId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/proceso/productosAgrupados?companiaId=${companiaId}`);
    }

    buscarGruposProductosParaCajaComandas(companiaId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/proceso/gruposProductosParaCajaComandas?companiaId=${companiaId}`);
    }

    buscarClientes(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarCliente?datoBusca=${text}`;
        return this.http
            .get(url)
            .pipe(
                map(data => data[0].map(item => {
                    return {
                        display: item.nombreCliente,
                        value: item.clientesId
                    }
                }))
            );
    }

    buscarClientesPorCompania(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarClientesPorCompania?datoBusca=
                     ${text}&companiasId=${this.companiaId}`;
        // return this.http
        //     .get(url)
        //     .map(data => data.json()[0].map(item => {
        //         return {
        //             display: item.nombreCliente,
        //             value: item.clientesId
        //         }
        //     }));
        return this.http
            .get(url)
            .pipe(map(data => data[0].map(item => {
                return {
                    display: item.nombreCliente,
                    value: item.clientesId
                }
            })))
    }

    buscarClientesTelefono(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarClienteTelefono?datoBusca=${text}`;
        return this.http
            .get(url)
            .pipe(map(data => data[0].map(item => {
                return {
                    display: item.nombreCliente,
                    value: item.clientesId
                }
            })));
    }

    buscarClientesTelefonoPorCompania(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarClientesTelefonoPorCompania?datoBusca=
                     ${text}&companiasId=${this.companiaId}`;
        return this.http
            .get(url)
            .pipe(map(data => data[0].map(item => {
                return {
                    display: item.nombreCliente,
                    value: item.clientesId
                };
            })));
    }


    buscarUsuario(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarUsuario?datoBusca=${text}`;
        return this.http
            .get(url)
            .pipe(map(data => data[0].map(item => {
                return {
                    display: item.nombreUsuario,
                    value: item.usuariosId
                };
            })));
    }

    buscarUsuarioPorEmpresaFiltro(text) {
        const url = `${environment.URL_SERVICIOS_LOCALES}/proceso/buscarUsuarioPorEmpresaFiltro?datoBusca=
                     ${text}&companiasId=${this.companiaId}`;
        return this.http
            .get(url)
            .pipe(map(data => data[0].map(item => {
                return {
                    display: item.nombreUsuario,
                    value: item.usuariosId
                };
            })));
    }


    // imprimirRecibo(text) {
    //     const url = this.urlPrintService || environment.URL_SERVICIOS_LOCALES_IMPRESORA;

    //     return this.http
    //         .post(url, { text, texto: text, cut: true, cash: true }, this.httpOptions);
    // }

    guardarVenta(laVenta, datosAdicionales, elaboradosContenido) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            laVenta: JSON.stringify(laVenta),
            datosAdicionales: JSON.stringify(datosAdicionales),
            elaboradosContenido,
        };
        return this.http.post(`${url}/procesos/almacenarVentaDiaria`, datos);
    }

    buscarClientesAll(companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId
        };
        return this.http.post(`${url}/procesos/buscarClientesPorCompaniaAll`, datos);
     }


     imprimirComandasMesas(codigo, companiasId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            elRecibo: codigo,
            companiasId: JSON.stringify(companiasId)
        };
        return this.http
            .post(`${url}/procesos/imprimirComandasMesas`, datos);
    }

    imprimirRecibo(text) {
        const url = this.urlPrintService || environment.URL_SERVICIOS_LOCALES_IMPRESORA;
        return this.http
            .post(environment.URL_SERVICIOS_LOCALES_IMPRESORANUEVA_VERSION, { text, texto: text, cut: true, cash: true }, this.httpOptions);
    }

     buscarDomiciliosPorCompania(companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId
        };
        return this.http.post(`${url}/procesos/buscarDomiciliosPorCompania`, datos);
      }
     buscarUsuarioPorEmpresaDomicilios(companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId
        };
        return this.http.post(`${url}/procesos/buscarUsuarioPorEmpresaDomicilios`, datos);
     }

     buscarVentasdiariasCaja(id: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId: id
        };
        return this.http
            .post(`${url}/procesos/buscarVentasdiariasCaja`, datos);
    }


     buscarGruposCompaniasId(companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId
        };
        return this.http.post(`${url}/procesos/buscargruposProductos`, datos);
   }

//    guardarTodo(data: any) {
//     const url = environment.URL_SERVICIOS_LOCALES;
//     return this.http.post(`${url}/procesos/query`, data);
//   }
  guardarTodo(data: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.post(`${url}/query`, data, this.httpOptions);
  }

    imprimirComanda(codigo, companiasId, descripcion) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            elRecibo: codigo,
            companiasId: JSON.stringify(companiasId),
            descripcion
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/imprimirComandas`, datos, {headers});
    }

    // imprimirComandasMesas(codigo, companiasId) {
    //     const url = environment.URL_SERVICIOS_LOCALES;
    //     const datos = {
    //         elRecibo: codigo,
    //         companiasId: JSON.stringify(companiasId)
    //     };
    //     const headers = new HttpHeaders({'Content-Type': 'application/json'});
    //     return this.http
    //         .post(`${url}/proceso/imprimirComandasMesas`, datos, {headers});
    // }

    imprimirComandasMesasEmpty(companiasId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            companiasId: JSON.stringify(companiasId)
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/imprimirComandasMesasEmpty`, datos, {headers});
    }



    imprimirFactura(consecutivo, companiasId) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            elRecibo: consecutivo,
            companiasId: JSON.stringify(companiasId)
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/imprimirReciboCaja`, datos, {headers});
    }



    imprimirComandaBlue(codigo: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=imprimirReciboCajaComandas&elRecibo=
                              ${codigo}&companiasId=${companiasId}`);
    }

    imprimirC() {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`sartprintbluetooth://print/CiAgICAgICAgICAgICAgU0FSQWBTCiAgICAgICAgICBOSVQ6IDQzNjE0MzM3LTkKICAgICAgIFJFR0lNRU4gU0lNUExJRklDQURPCiAgICAgICBESVI6IENSIDgzICMgMzIgQiAxNwogICAgICAgICAgVEVMOiAyOTQgMTIgOTEKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKRmFjdHVyYSBOcm8uICAgOiAxOTQ3CkZlY2hhIHZlbnRhICAgIDogMjAxOC0xMS0yOSAyMzozOQpVc3VhcmlvICAgICAgICA6IFNBUkEKQ2xpZW50ZTogQ0xJRU5URSBPQ0FTSU9OQUwKSWRlbnRpZmljYWNpw7NuOiA5OTk5CkRpcjogClRlbDogCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCk5vbWJyZSBQcm9kdWN0byAgICAgICAgICAgICAgICAgICAgCkNhbnQuICAgIFZhbG9yIFVuaS4gICAgIFZhbG9yIFRvdGFsCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkdBU0VPU0EgMjUwICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAzICAgICAgMi4wMDAsMDAgICAgICAgIDYuMDAwLDAwClBJTFNFTiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwCkFHVUlMQSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwClVOSURBRCBCT1NUT04gLSBHUkVFTiAgICAgICAgICAgICAgCiAgICA0ICAgICAgICA1MDAsMDAgICAgICAgIDIuMDAwLDAwCk1JQ0hFTEFEQSBQSUxTRU4gICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC41MDAsMDAgICAgICAgIDQuNTAwLDAwCkNMSUNMRSAgUEVRVUXDkU8gQUREQU1TICAgICAgICAgICAgIAogICAgNCAgICAgICAgMjAwLDAwICAgICAgICAgIDgwMCwwMApWSVZFIDEwMCBQRVFVRcORTyAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICAyLjAwMCwwMCAgICAgICAgNC4wMDAsMDAKVklWRSAxMDAgR1JBTkRFICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAyLjUwMCwwMCAgICAgICAgMi41MDAsMDAKR0FTRU9TQSAgUE9TVE9CT04gMS41ICAgICAgICAgICAgICAKICAgIDMgICAgICA0LjAwMCwwMCAgICAgICAxMi4wMDAsMDAKQk9URUxMQSBBR1VBICAgICAgICAgICAgICAgICAgICAgICAKICAgIDcgICAgICAzLjAwMCwwMCAgICAgICAyMS4wMDAsMDAKVElOVE8gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAyLjAwMCwwMCAgICAgICAgMi4wMDAsMDAKR0FTRU9TQSA0MDAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAzLjAwMCwwMCAgICAgICAgMy4wMDAsMDAKR0FUT1JBREUgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICA0LjAwMCwwMCAgICAgICAgNC4wMDAsMDAKQUdVQSBTQUJPUklaQURBIEgyTyAgICAgICAgICAgICAgICAKICAgIDEgICAgICA0LjAwMCwwMCAgICAgICAgNC4wMDAsMDAKQ0FTRVJBUyAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICA0LjAwMCwwMCAgICAgICAgOC4wMDAsMDAKQk9NQk9NQlVNICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICAgIDUwMCwwMCAgICAgICAgICA1MDAsMDAKSEVJTkVLRU4gICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDIgICAgICA3LjAwMCwwMCAgICAgICAxNC4wMDAsMDAKMS8yIExVQ0tZICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgIDEgICAgICA1LjAwMCwwMCAgICAgICAgNS4wMDAsMDAKVFJJREVOVCBJTkRJVklEVUFMICAgICAgICAgICAgICAgICAKICAgIDIgICAgICAgIDMwMCwwMCAgICAgICAgICA2MDAsMDAKQlJFVEHDkUEgTk8gUkVUT1JOQUJMRSAgICAgICAgICAgICAgCiAgICAxICAgICAgMy4wMDAsMDAgICAgICAgIDMuMDAwLDAwClVOSURBRCBMVUNLWSAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgICA2MDAsMDAgICAgICAgICAgNjAwLDAwCkhFUlNIRVlTICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgNC4wMDAsMDAgICAgICAgIDQuMDAwLDAwClRSSURFTlQgWCAzICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMS4wMDAsMDAgICAgICAgIDEuMDAwLDAwCkNBTkRFTEEgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMi4wMDAsMDAgICAgICAgIDIuMDAwLDAwClJPTiBTRU5DSUxMTyAgICAgICAgICAgICAgICAgICAgICAgCiAgICAyICAgICAgNS4wMDAsMDAgICAgICAgMTAuMDAwLDAwCkdBU0VPU0EgMzUwICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMy4wMDAsMDAgICAgICAgIDMuMDAwLDAwClNPREEgTUlDSEVMQURBICAgICAgICAgICAgICAgICAgICAgCiAgICAyICAgICAgNC4wMDAsMDAgICAgICAgIDguMDAwLDAwCjEvMiBCT1NUT04gLSBHUkVFTiAgICAgICAgICAgICAgICAgCiAgICA3ICAgICAgNC4wMDAsMDAgICAgICAgMjguMDAwLDAwClVOSURBRCBNQUxCT1JPICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgICA2MDAsMDAgICAgICAgICAgNjAwLDAwClNBTCBERSBGUlVUQVMgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMy41MDAsMDAgICAgICAgIDMuNTAwLDAwCkFMS0FTRUxUWkVSICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAxICAgICAgMi41MDAsMDAgICAgICAgIDIuNTAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tClNVQlRPVEFMICAgICAgICAgICA1OSAgICAxNjguMDYwLDAwCklNUE9DT05TVU1PIDEsMDAlICAgICAgICAgICAgIDQwLDAwClRPVEFMIEEgUEFHQVIgICAgICAgICAgICAxNjguMTAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkVGRUNUSVZPICAgICAgICAgICAgICAgICAxNjguMTAwLDAwCkNBTUJJTyAgICAgICAgICAgICAgICAgICAgICAgICAwLDAwCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tCkdyYWNpYXMgcG9yIHN1IENvbXByYQogICAgSW1wcmVzbyBwb3IgUmVzdEFwcC4KICAgICAoKzU3KSAzMjAgNzMzMDI1NAogIGFuZHJlc25hcmFuam9AYWZuYXJxdWkuY29tCgoK`);
    }

    imprimirComandaBlueafn(codigo: any, companiasId: any) {
        const url = environment.URL;
        const datos = {
            elRecibo: codigo,
            companiasId
          };
        const headers = new HttpHeaders({'Content-Type': 'application/json',
        Authorization: `Bearer ${this.token}`});
        // return this.http
        //     .post(`${url}/procesos/imprimirReciboCajaComandas`, datos);
        return this.http
            .post(`${url}/procesosafn/imprimirReciboCajaComandas`, datos, {headers});
    }

    imprimirBlueRedirect(data: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            nombre: JSON.stringify(data)
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.post(`${url}/blueRedirect`, datos, {headers});
    }

    guardarComandas(data, codigo) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            datos: JSON.stringify(data),
            codigo
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/almacenarComandas`, datos, {headers});
    }

    
        abrirPagina(nombre:string){
           let promise = new Promise((resolve,reject)=> {
            this.store.select('rutas')
            .subscribe((item) => {
                console.log(item);
                console.log('rutas')
                let data = 0
                const datos = JSON.parse(item.rutas.nombre);
                for(var i=0;i < datos.length;i++){
                  if(datos[i].nombre == nombre){
                    data = -1
                    return resolve(true);
                  }
                }
                return resolve(false);
            }, (error ) => {
              console.log(error);
              return reject(false);
            });
           })

           return promise
          }
    

    guardarComandasMesero(data, codigo, descripcion) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            datos: JSON.stringify(data),
            codigo,
            descripcion
        };
        // const headers = new HttpHeaders({'Content-Type': 'application/json',
        //                                   Authorization: `Bearer ${this.token}`});
        // return this.http.post(`${url}/proceso/almacenarComandasMesero`, datos, {headers});
        return this.http.post(`${url}/procesos/almacenarComandasMeseroTurnos`, datos);

    }

    buscarComandas(codigo: any, companiasId: any, estado: string) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandas&mesa=
                              ${codigo}&companiasId=${companiasId}&estado=${estado}`);
    }

    buscarComandasMesero(codigo: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasMesero&mesa=
                              ${codigo}&companiasId=${companiasId}`);
    }

    buscarComandasturnoPorCompania(turno: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasturnoPorCompania&turno=
                              ${turno}&companiasId=${companiasId}`);
    }

    buscarComandasturnoPorCompaniaAsignados(companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasturnoPorCompaniaAsignados&companiasId=${companiasId}`);
    }
    

    buscarComandasDomicilio(codigo: any, companiasId: any, estado: string) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasDomicilios&codigo=
                              ${codigo}&companiasId=${companiasId}&estado=${estado}`);
    }

    ActualizarColorComandasDomicilio(codigo: any, companiasId: any, color: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=ActualizarColorComandasDomicilio&codigo=
                              ${codigo}&companiasId=${companiasId}&color=${color}`);
    }

    buscarComandasPorId(id: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasPorId&id=${id}`)

    }

    buscarComandasFactura(codigo: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasFactura&codigo=${codigo}`)

    }

    buscarComandasFacturaPorCompania(codigo: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasFacturaPorCompania&codigo=${codigo}&companiasId=${this.companiaId}`)

    }



    buscarComandasDomicilios() {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarComandasDomicilios`)

    }

    buscarParametrosGenerales(companiaId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http.get(`${url}/query?name=BuscarparametrosGenerales&companiaId=
                              ${companiaId}&usuariosId=${this.usuarioId}`);
    }

    actualizarComandas(comanda: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        
        return this.http.put(`${url}/query`, comanda, this.httpOptions);
    }
    genearNotificacionComandas(titulo, mensaje) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/notificacion?titulo=${titulo}&mensaje=${mensaje}`)
    }

    cancelarComandas(item: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=cancelarComandas&codigo=${item.comandasId}`, this.httpOptions);
    }

    cancelarComandasPorCompanias(item: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=cancelarComandasPorCompanias&codigo=
                              ${item.comandasId}&companiasId=${companiasId}`, this.httpOptions);
    }

    eliminarComandasPorCompanias(codigo: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        // return this.http.get(`${url}/query?name=eliminarComandasPorCompanias&codigo=
        //                       ${codigo}&companiasId=${companiasId}`, this.httpOptions);
        const datos: any = {
            codigo,
            companiasId
        };
        return this.http.post(`${url}/procesos/eliminarComandasPorCompanias`, datos);
    }

    // getComandasVacio(companiasId: any, usuariosId: any, clientesId: any, descripcion: any) {
    //     const url = environment.URL_SERVICIOS_LOCALES;
    //     const datos: any = {
    //         companiasId,
    //         usuariosId,
    //         clientesId,
    //         descripcion
    //     };
        
    //     return this.http.post(`${url}/procesos/GuardarDomiciliosAppComandasVacio`, datos);
    // }

    getComandasVacio(companiasId: any, usuariosId: any, clientesId: any, descripcion: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos: any = {
            companiasId
        };
        return this.http.post(`${url}/procesos/BuscarTurnosAppComandasVacio`, datos);
    }
    
    buscarTurnosDomiciliosAppComandas(companiasId, numeroPedido) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos: any = {
            companiasId,
            numeroPedido
        };
        return this.http.post(`${url}/procesos/buscarTurnosDomiciliosAppComandas`, datos);
    }

    buscarDenominaciones() {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=denominaciones`)
      }

    buscarFactura(val: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=reemprimirFacturas&companiasId=${val}`, this.httpOptions);
    }

    buscarFacturaCuentaPorCobrar(elRecibo: any, val: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=imprimirReciboCaja&elRecibo=
                              ${elRecibo}&companiasId=${val}`, this.httpOptions)
    }

    buscarVentasDiarias(elRecibo: any, val: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=imprimirReciboCaja&elRecibo=${elRecibo}&companiasId=${val}`)

    }

    devolverVenta(val: any, consecutivo: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=devolverVenta&companiasId=${val}&consecutivo=${consecutivo}`)
    }

    buscarCuentaPorCobrar(data) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            jsonFiltros: JSON.stringify(data)
        };
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/buscarCuentasPorCobrarConSaldo`, datos, {headers});
    }

    buscarCuentaPorCobrarValorTotal(companiasId: any, clientesId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=buscarCuentaPorCobrarValorTotal&companiasId=
                              ${companiasId}&clientesId=${clientesId}`, this.httpOptions);
    }

    almacenarPagoCuentaPorCobrar(data) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            elPago: JSON.stringify(data)
        }
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        return this.http
            .post(`${url}/proceso/almacenarPagoCuentaPorCobrar`, datos, {headers});
    }

    buscarinformePorFechas(valor: any, companiasId: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        return this.http.get(`${url}/query?name=informeDeVentasDiariasPorCostos&fechaInforme=
                              ${valor}&companiasId=${companiasId}`, this.httpOptions);
    }

    buscarinformeLocal(data: any, nombre: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos = {
            data: JSON.stringify(data),
            nombre: 'VentasDiariasPorCostos'
        };
        this.http.post(`${url}/informes`, datos, this.httpOptions);
        this.storage.set('imprimirArchivo', 'VentasDiariasPorCostos');
    }


    guardarProductosDesconectado(result, nombreProcedimiento) {

        const cuantos = result.length - 1;

        const dataRealJson = [];

        const url = environment.URL_SERVICIOS_LOCALES;
        const nuevaData = result.filter(e => e.actualizado.indexOf('S') !== -1)
        if (nuevaData.length > 0) {
            const datos: any = {
                datos: JSON.stringify(nuevaData)
            };
            const headers = new HttpHeaders({'Content-Type': 'application/json'});
            return this.http.post(`${url}/procesos/${nombreProcedimiento}`, datos, {headers});
        } else {
            const datos: any = {
                datos: ''
            }

            return this.http.post(`${url}/procesos/respuestaDesconectado`, datos);
        }
    }

    sincronizarDatosSubidosModoDesconectado(companiasId: any, fechaSincroniza: any) {

        const url = environment.URL_SERVICIOS_LOCALES;
        const datos: any = {
            companiasId,
            fechaSincroniza
        };
        return this.http.post(`${url}/procesos/sincronizarDatosSubidosModoDesconectado`, datos, this.httpOptionsToken);
    }

    guardarVentasDiariasDesconectado(jsonLaVenta: any, jsonDatosAdicionales: any) {

        const url = environment.URL_SERVICIOS_LOCALES;
        const datos: any = {
            jsonLaVenta,
            jsonDatosAdicionales
        };
        return this.http.post(`${url}/procesos/guardarVentasDiariasDesconectado`, datos, this.httpOptionsToken);
    }

    ventasDiariasSinSincronizar(data: any) {

        const url = environment.URL_SERVICIOS_LOCALES;

        let datos: any = {
            datos: JSON.stringify(data)
        };

        return this.http.post(`${url}/procesos/guardarVentasDiariasDesconectado`, datos, this.httpOptionsToken);
    }

    trasladarMesaPorCompaniaDomicilios(mesaActual: any, mesaNueva: any, companiasId: any, turno: any) {
        const url = environment.URL_SERVICIOS_LOCALES;
        const datos: any = {
            mesaActual,
            mesaNueva,
            companiasId,
            turno
        };
        return this.http.post(`${url}/procesos/trasladarMesaPorCompaniaDomicilios`, datos);
        // return this.http.get(`${url}/query?name=trasladarMesaPorCompania&mesaActual=
        //                       ${mesaActual}&mesaNueva=${mesaNueva}&companiasId=${companiasId}`);
    }
}
