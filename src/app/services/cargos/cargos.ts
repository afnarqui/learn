import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from "rxjs/operators";
import { URL_SERVICIOS_LOCALES } from '../../../config/url.servicio'
import { Storage } from '@ionic/storage'

@Injectable({ providedIn: 'root' })
export class CargosServices {

  token:string
  constructor(public http: HttpClient, private storage: Storage) {
    this.storage.get('token').then((val) => {
      this.token = val
    });
    this.buscar()
  }

  buscar() {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=cargos`)
    .pipe(
    map((data) => {
      return data}))
  }

  buscarCargosPorEmpresa(companiasId:any){
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=buscarCargosPorEmpresa&companiasId=${companiasId}`)
    .pipe(
      map((data) => {
        return data}))
  }      

  buscarTodo(name:string) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=${name}`)
    .pipe(
      map((data) => data))
  }

  buscarContenedores(companiasId:any){
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=buscarContenedores&companiasId=${companiasId}`)
    .pipe(
      map((data) => data))
  }    

  guardar(cargo: any) {
    let url = URL_SERVICIOS_LOCALES
    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.post(`${url}/query`, cargo, { headers: headers })
    .pipe(
      map((data) => data))
  }

  guardarTodo(data: any) {
    let url = URL_SERVICIOS_LOCALES
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post(`${url}/query`, data, { headers: headers })
    .pipe(
      map((data) => data))
  }

  actualizar(cargo: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.put(`${url}/query`, cargo, {headers: headers})
  }

  actualizarTodo(data: any) {
    let url = URL_SERVICIOS_LOCALES
   
    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.put(`${url}/query`, data, {headers: headers})
  }


  eliminar(cargo: any) {
    let url = URL_SERVICIOS_LOCALES
    const headers = new HttpHeaders({'Content-Type':'application/json'});

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this.token
      })
    };
    
    return this.http.delete(`${url}/query?name=dataDynamicCrud&queHago=E&cargosId=${cargo.cargosId}&nombreTabla=cargos`, {headers: headers} )
  }

  eliminarContenedor(data: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.delete(`${url}/query?name=dataDynamicCrud&queHago=E&contenedorId=${data.contenedorId}&nombreTabla=contenedor`, { headers: headers })
  }

}
