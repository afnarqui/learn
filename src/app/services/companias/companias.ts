import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from "rxjs/operators";

import { Storage } from '@ionic/storage'

import { URL_SERVICIOS_LOCALES } from '../../../config/url.servicio'
import { environment } from './../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CompaniasServices {
  public companiasId:string = null;
  constructor(public http: HttpClient,
              public storages:Storage) {
    this.buscar()
  }
  buscar() {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=companias`)
    .pipe(
      map((data:any) => data.json())
    )
  }
  buscarCompaniasId(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/proceso/buscarCompaniasId?companiasId=${companiasId}`)
    .pipe(map((data:any) => data.json()))
  }


  guardar(compania: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.post(`${url}/query`, compania, {headers:headers})
      .pipe(map((data:any) => data.json()))
  }

  actualizar(compania: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});

    return this.http.put(`${url}/query`, compania, {headers:headers})
  }

  eliminar(compania: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.delete(`${url}/query?name=dataDynamicCrud&queHago=E&companiasId=${compania.companiasId}&nombreTabla=companias`, {headers:headers})
  }
  datosStorage(){
      console.log("companiasId guardar storage " , this.companiasId);
      if( this.companiasId ){
        localStorage.setItem("companiasId", this.companiasId);
        return this.companiasId
      }else{
        localStorage.removeItem("companiasId");
        console.log("companiasId borrada");
        return this.companiasId
      }
  }
  cargarcompaniaId(id:any){
    this.companiasId=id
    this.datosStorage()
  }
buscarId(){
  let promesa = new Promise((resolve,reject)=>{

      let id = localStorage.getItem("companiasId");
        if(id){
          resolve(id)
        }
      else{
        localStorage.removeItem("companiasId");
        console.log("companiasId borrada");
        this.companiasId = null
        reject(false)
      }
  })
  return promesa
}

devolverVarios(keys: string[]) {
  const promises = [];

  keys.forEach( key => promises.push(this.storages.get(key)) );

  return Promise.all(promises).then( values => {
    const result = {};
    
    values.map( (value, index) => { 
      result[keys[index]] =value; 
    });

    return result;
  });
}


guardarInformacionMododesconectado(companiasId: any){
let url = URL_SERVICIOS_LOCALES

const headers = new HttpHeaders({'Content-Type':'application/json'});
let datos: any = {
    "companiaId": companiasId,
    "queHago":"G",
    "name":"guardarInformacionMododesconectado"
  }
return this.http.put(`${url}/query`,datos, {headers:headers})
}

buscarCodigoBarra(companiasId:any,codigo:any) {
  const url = environment.URL_SERVICIOS_LOCALES;
  const datos = {
      companiasId,
      codigo
  };
  return this.http.post(`${url}/procesos/buscarCodigoBarra`, datos);
}

buscarCodigoUsuarioValido(codigo:any) {
  let url = URL_SERVICIOS_LOCALES
  
  return this.http.get(`${url}/query?name=buscarCodigoUsuarioValido&codigo=${codigo}`)
  .pipe(map((data:any) => data.json()))
}

}
