import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from "rxjs/operators";
import { URL_SERVICIOS_LOCALES } from '../../../config/url.servicio'

@Injectable({ providedIn: 'root' })
export class DenominacionesServices {
  constructor(public http: HttpClient) {
    this.buscar()
  }

  buscar() {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=denominaciones`)
      .pipe(map((data) => data))
  }

  buscarCorreo(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=buscarcorreos&companiasId=${companiasId}`)
      .pipe(map((data) => data))
  }

  // guardar(denominaciones: any) {
  //   let url = URL_SERVICIOS_LOCALES
  //   let headers = new Headers({
  //     'Content-Type': 'application/json'
  //   })
  //   let options = new RequestOptions({
  //     headers: headers
  //   })

  //   return this.http.post(`${url}/query`, denominaciones, options)
  //     .map((data) => data.json())
  // }

  // actualizar(denominaciones: any) {
  //   let url = URL_SERVICIOS_LOCALES
  //   let headers = new Headers({
  //     'Content-Type': 'application/json'
  //   })
  //   let options = new RequestOptions({
  //     headers: headers
  //   })
  //   //?name=dataDynamicCrud&denominacionesId=${denominaciones.denominacionesId}&codigodenominaciones=${denominaciones.codigodenominaciones}&nombredenominaciones=${denominaciones.nombredenominaciones}&nombreTabla=denominaciones
  //   return this.http.put(`${url}/query`, denominaciones, options)
  // }
  // eliminar(denominaciones: any) {
  //   let url = URL_SERVICIOS_LOCALES
  //   let headers = new Headers({
  //     'Content-Type': 'application/json'
  //   })
  //   let options = new RequestOptions({
  //     headers: headers
  //   })
  //   //?name=dataDynamicCrud&denominacionesId=${denominaciones.denominacionesId}&nombreTabla=denominaciones
  //   return this.http.delete(`${url}/query?name=dataDynamicCrud&queHago=E&denominacionesId=${denominaciones.denominacionesId}&nombreTabla=denominaciones`, options)
  // }
}
