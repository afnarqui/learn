import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from "rxjs/operators";
import { URL_SERVICIOS_LOCALES } from '../../../config/url.servicio'

@Injectable()
export class GraficasServices {

  constructor(public http: HttpClient) {
    this.buscar()
  }

  buscar() {
    let url = URL_SERVICIOS_LOCALES
      return this.http.get(`${url}/query?name=ventasDiarias`)
      .pipe(
        map((data:any) => data.json())
      )
  }

  buscarVentasDiarias(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    companiasId = companiasId === null ? 1 : companiasId;
    return this.http.get(`${url}/query?name=ventasDelDiaPorHora&companiasId=${companiasId}`)
    .pipe(
      map((data:any) => {
        // data.json()
        return data})
    ) 
  }

  buscarVentasSemanalesPorDias(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=ventasSemanalesPorDia&companiasId=${companiasId}`)
    .pipe(
      map((data:any) => data.json())
    ) 
  }

  buscarventasMensualesPorSemanas(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=ventasMensualesPorSemana&companiasId=${companiasId}`)
    .pipe(
      map((data:any) => data.json())
    ) 
  }

  buscarventasAnualesPorMes(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=ventasAnualesPorMes&companiasid=${companiasId}`)
    .pipe(
      map((data:any) => { 
        return data})
    ) 
  }


  buscarProductos() {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/proceso/buscarProductosTodos`)
    .pipe(
      map((data:any) => {
        return data[0]})
    ) 
  }  

  buscarProductosTodos(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/proceso/buscarProductosTodosInventario?companiasId=${companiasId}`)
    .pipe(
      map((data:any) => data)
    ) 
  }  
  actualizarProducto(producto: any) {
    let url = URL_SERVICIOS_LOCALES

    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.put(`${url}/query`, producto, {headers:headers})
  }
}
