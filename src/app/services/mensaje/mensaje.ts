import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MensajeProvider {

  private init = 450;
  private source = new BehaviorSubject<number>(this.init);
  public llamarevento$ = this.source.asObservable();

  constructor() { }

  restartCountdown(init?) {
    this.source.next(init);
  }
}
