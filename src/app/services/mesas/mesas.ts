import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class MesasServices {
  token: string;
  urlPrintService: string;
  companiasId: number;
  constructor(public http: HttpClient, private storage: Storage) {
    this.storage.get('token').then((val) => {
      this.token = val;
    });

    this.storage.get('companiasId').then((val) => {
      this.companiasId = val;
    });
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  guardar(ubicacionesPorSalones: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.post(`${url}/query`, ubicacionesPorSalones, this.httpOptions);
  }

  eliminar(numeroMesa: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/proceso/eliminarMesas?numeromesa=${numeroMesa}`);
  }

  ActualizarComandasSinPedido(companiasId: any, turnoNuevo: any, numeroPedido: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    const datos: any = {
      companiasId,
      turnoNuevo,
      numeroPedido
  };
    return this.http.post(`${url}/procesos/ActualizarComandasSinPedido`, datos);
  }

  eliminarTodas() {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/proceso/eliminarMesasTodas`);
  }

  eliminarMesasTodasPorCompanias(companiasId: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/proceso/eliminarMesasTodasPorCompanias?companiasId=${companiasId}`);
  }

  buscar() {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/query?name=mesas`);
  }

  buscarMesasPorCompania(companiasId: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    // buscarMesasPorCompania
    // return this.http.get(`${url}/query?name=buscarMesasPorCompaniaTablet&companiasId=${companiasId}`);
    const data = {
      companiasId
    };
    return this.http.post(`${url}/procesos/buscarMesasPorCompaniaTablet`, data);
  }

  buscarUnicas() {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/proceso/buscarMesas`);
  }

  buscarMesasPorCompaniaUnico(companiasId: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/proceso/buscarMesasPorCompaniaUnico?companiasId=${companiasId}`);
  }

  buscarListamesasPorCompania(valor) {
    const url = environment.URL_SERVICIOS_LOCALES;
    const datos = {
      companiasId: valor
    };

    return this.http.post(`${url}/procesos/buscarListamesasPorCompania`, datos);
   
    // return this.http.get(`${url}/query?name=buscarListamesasPorCompania&companiasId=${valor}`);
  }
}
