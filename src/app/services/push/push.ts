import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from "rxjs/operators";
import { URL_SERVICIOS_LOCALES } from '../../../config/url.servicio'
import { Storage } from '@ionic/storage'

@Injectable({ providedIn: 'root' })
export class PushServices {
  token:string
  usuarioId: number
  constructor(public http: HttpClient, private storages: Storage) {
    this.storages.get('token').then((val) => {
      this.token = val
  });

  this.storages.get('usuariosId').then((usuario) => {
    this.usuarioId = usuario
  });

  }

  buscar(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/proceso/buscarPush?companiasId=${companiasId}`)
    .pipe(map((data) => data))
  }

  procesarDatosPush ( companiasId){
    let url = URL_SERVICIOS_LOCALES
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': 'Bearer ' + this.token
      })
    };
    
    const datos = {
        "companiasId": JSON.stringify(companiasId)
    }
 
    return this.http
        .post(`${url}/proceso/procesarDatosPush`, datos, httpOptions)
}

eliminar(data: any) {
    let url = URL_SERVICIOS_LOCALES
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.http.delete(`${url}/query?name=dataDynamicCrud&queHago=E&pushId=${data}&nombreTabla=push`, {headers: headers})
  }

  buscarParametrosGenerales(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
  
    const headers = new HttpHeaders({'Content-Type':'application/json'});
  
    return this.http.get(`${url}/proceso/BuscarparametrosGenerales?companiaId=${companiasId}&usuariosId=${this.usuarioId}`, {headers: headers})
    .pipe(map((data) => data))
  }

  buscarParametrosGeneralesParaMaestro(companiasId:any) {
    let url = URL_SERVICIOS_LOCALES
  
    const headers = new HttpHeaders({'Content-Type':'application/json'});
  
    return this.http.get(`${url}/proceso/buscarParametrosGeralesMaestro?companiaId=${companiasId}`, {headers:headers})
    .pipe(map((data) => data))
  }

  actualizarParametrosGenerales(parametrosGenerales: any) {
    let url = URL_SERVICIOS_LOCALES
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    //?name=dataDynamicCrud&paisesId=${pais.paisesId}&codigoPais=${pais.codigoPais}&nombrePais=${pais.nombrePais}&nombreTabla=paises
    return this.http.put(`${url}/query`, parametrosGenerales, {headers:headers})
  }

  buscarTodoUbicacion(name:string,contenedorId:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/query?name=${name}&contenedorId=${contenedorId}`)
    .pipe(map((data) => data))
  }


  cualquiercosa(afn:any) {
    let url = URL_SERVICIOS_LOCALES
    return this.http.get(`${url}/proceso/cualquiercosa?afn=${afn}`)
    .pipe(map((data) => data))
  }

}
