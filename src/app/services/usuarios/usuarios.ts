import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UsuariosProvider {

  constructor(public http: HttpClient) {
    this.buscar();
  }

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
  };

  buscar() {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/query?name=usuarios`);
  }


  buscarUsuarioPorEmpresa(companiasId: any){
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/query?name=buscarUsuarioPorEmpresa&companiasId=${companiasId}`);
  }

  buscarUsuariosCompanias(codigo: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.get(`${url}/query?name=buscarUsuariosCompaniasNoExistente&companiasId=${codigo}`);
  }

  buscarUsuarioAdministrador(codigo: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    const datos = {
      usuariosId: codigo
    };

    return this.http.post(`${url}/procesos/buscarUsuarioAdministrador`, datos);
    // return this.http.get(`${url}/query?name=buscarUsuarioAdministrador&usuariosId=${codigo}`);
  }

  guardar(usuario: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.post(`${url}/registro`, usuario, this.httpOptions);
  }

  actualizar(usuarios: any) {
    const url = environment.URL_SERVICIOS_LOCALES;
    return this.http.put(`${url}/query`, usuarios, this.httpOptions);
  }
}
