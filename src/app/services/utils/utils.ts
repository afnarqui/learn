import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

declare var Swal:any

@Injectable({ providedIn: 'root' })
export class UtilsServices {

  constructor() { }

  mensajeMuestro(title:any,text:any,icon:any){
    Swal.fire({
      title: title,
      text: text,
      icon: icon,
      confirmButtonText: 'Aceptar'
    });
  }


  mensajeConfirmacionDos(title:any,text:any,icon:any,confirmButtonText:any,cancelButtonText:any){
    return Swal.fire({
      title: title,
      text: text,
      icon: icon,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: confirmButtonText,
      cancelButtonText: cancelButtonText
    }).then((result) => {
      return result.value
    })
  }

  mostrarMensajeHtml(textoBotonConfirmar: string, titulo: string, icono: string, stringHtml: string){
    Swal.fire({
      title: '<strong> ' + titulo + '</strong>',
      icon: icono,
      html: stringHtml,
      showCloseButton: true,
      focusConfirm: false,
      confirmButtonText:  textoBotonConfirmar,
      confirmButtonColor: '#3085d6',
      timerProgressBar: true,
      padding: '6rem'
    })
  }

  mensajeConfirmacion(title:any,text:any,icon:any,buttons: string[],dangerMode:any){
   
    return Swal({
      title: title,
      text: text,
      icon: icon,
      buttons: buttons,
      dangerMode: dangerMode
    })
  }


  mostrarReciboDeCajaEnPantalla(elNroFactura: any, elReciboMuestra: any){
    let recibo =  `<div align="left" style='font-family: monospace;'> `
    for (let index:any = 0; index < elReciboMuestra.length; index++) {
      recibo += elReciboMuestra[index]['reciboCaja'].replace(/\ /g,'&nbsp;') + '<br/>'
    }
    recibo += '</div>'

    let tituloRecibo = elNroFactura != '' ? 'Factura Número: ' + elNroFactura : 'Visualización Factura'

    this.mostrarMensajeHtml('Salir',tituloRecibo,'',recibo)
  }

}
