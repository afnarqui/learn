import { Action } from '@ngrx/store';
import { Rutas } from '../models/ruta/ruta.models';

export const SET_RUTAS = '[Ingresar rutas] Set rutas';
export const UNSET_RUTAS = '[Eliminar rutas] UnSet rutas';


export class SetRutasAction implements Action {
    readonly type = SET_RUTAS;

    constructor(public rutas: Rutas) {}
}

export class UnsetRutasAction implements Action {
    readonly type = UNSET_RUTAS;
}

export type acciones = SetRutasAction
                       | UnsetRutasAction;