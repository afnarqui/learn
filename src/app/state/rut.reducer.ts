import * as fromRutas from './rut.accions'
import { Rutas } from '../models/ruta/ruta.models';

export interface RutasState {
    rutas: Rutas
}

const initState: RutasState = {
    rutas: null
};

export function rutasReducer( state = initState, action: fromRutas.acciones): RutasState {
    switch ( action.type) {
        case fromRutas.SET_RUTAS:
            return {
                rutas: {...action.rutas}
            };
        case fromRutas.UNSET_RUTAS:
            return {
                rutas: null
            };
        default:
            return state;
    }
}
