// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  MODO_DESCONECTADO: false,
  URL_SERVICIOS_LOCALES: '',
  URLs: '',
  URL: '',
  UAFN: '',
  URL_SERVICIOS_LOCALES_IMPRESORA: '',
  URL_SERVICIOS_LOCALES_IMPRESORANUEVA_VERSION: '',
  GESTION_PEDIDOS_SALON: false,
  firebaseConfig : {
    apiKey: "AIzaSyDD3bDqinxaYEmXxyy-YBeZpmJGCKvGeJU",
    authDomain: "sistemaadministrativodenegocio.firebaseapp.com",
    projectId: "sistemaadministrativodenegocio",
    storageBucket: "sistemaadministrativodenegocio.appspot.com",
    messagingSenderId: "486695262820",
    appId: "1:486695262820:web:5b46e0d384a9c338981d71"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
